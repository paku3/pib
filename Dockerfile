FROM python:3.10

WORKDIR /pib

COPY . .

RUN pip install -r requirements.txt

CMD python3 PB.py
