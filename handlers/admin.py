from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher.filters import Text
from aiogram import types, Dispatcher
from creat_b import dp, bot



#Хочу стать админом
global admin
admin = 0

async def get_admin(message: types.Message):
	global admin
	admin *= 0
	admin += message.from_user.id
	await bot.send_message(admin, 'Ты стал админом')



class FSMAdmin(StatesGroup):
	calVoLud = State()
	date = State()
	Telefon = State()
	Name = State()

async def mh_start(message: types.Message):
	await FSMAdmin.calVoLud.set()
	await message.reply('На скільки людей ви хочете забронювати столик?')

async def load_calVoLud(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['calVoLud'] = message.text 
	await FSMAdmin.next()
	await message.reply('Введить дату та час коли бажаєте (Зразок: 02.01. 10:50)')

async def load_date(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['date']= message.text
	await FSMAdmin.next()
	await message.reply('Номер вашего телефону')

async def load_Telefon(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['Telefon']= message.text
	await FSMAdmin.next()
	await message.reply('Як вас звати?')

async def load_name(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['Name']= message.text
	await message.reply('Ваше замовлення прийнято')
	async with state.proxy() as data:
		user_data = await state.get_data()
		if admin > 0:
			await  message.reply(f"Ваше замолення на  {user_data['date']} стола для {user_data['calVoLud']} людей, За {user_data['Name']} на розгляді. Ми до вас звернемося по телефону {user_data['Telefon']}. Дякуємо) ")
			await bot.send_message(admin, f"Була бронь на  {user_data['date']} стола для {user_data['calVoLud']} людей. Людину яка замовляла звуть: {user_data['Name']}. Треба звернутись по телефону {user_data['Telefon']} ")
		if admin <= 0:
			await message.reply(f"Зараз адміністратор не на місці, зачекайте та спробуйте ще раз, чи зателефонуйте по номеру:")	
	await state.finish()
#@dp.message_handler(state ='*', commands='отмена')
#@dp.message_handler(Text(equals='отмена', ignore_case=True), state='*')
#async def cancel_handler(message: types.Message, state: FSMContext):
#	current_state = await state.get_state()
#	if current_state is None:
#		return
#	await state.finish()
#	await message.reply('Ладно')

class FSMAdminK(StatesGroup):
	calVoLudK = State()
	date = State()
	Telefon = State()
	Name = State()

async def mрK_start(message: types.Message):
	await FSMAdminK.calVoLudK.set()
	await message.reply('На скільки людей ви хочете забронювати кімнату?')

async def load_calVoLudK(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['calVoLudK'] = message.text 
	await FSMAdminK.next()
	await message.reply('Введить дату та час коли бажаєте (Зразок: 02.01. 10:50)')

async def load_dateK(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['date']= message.text
	await FSMAdminK.next()
	await message.reply('Номер вашего телефону?')

async def load_TelefonK(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['Telefon']= message.text
	await FSMAdminK.next()
	await message.reply('Як вас звати?')

async def load_nameK(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['Name']= message.text
	await message.reply('Ваше замовлення прийнято')
	async with state.proxy() as data:
		user_data = await state.get_data()
		if admin > 0:
			await  message.reply(f"Ваше замолення на  {user_data['date']} кімнати для {user_data['calVoLudK']} людей, За {user_data['Name']} на розгляді. Ми до вас звернемося по телефону {user_data['Telefon']} ")
			await bot.send_message(admin, f"Була бронь на  {user_data['date']} кімнати для {user_data['calVoLudK']} людей.Людину яка замовляла звуть: {user_data['Name']}. Треба звернутись по телефону  {user_data['Telefon']} ")
		if admin <= 0:
			await message.reply(f'Зараз адміністратор не на місці, зачекайте та спробуйте ще раз, чи зателефонуйте по номеру:')
	await state.finish()







def Admin_Handlers_register(dp: Dispatcher):
	dp.register_message_handler(get_admin, text = ['Хочу стать админом'])
	dp.register_message_handler(mh_start, text =['Бронь столика'], state=None)
	dp.register_message_handler(load_calVoLud, state=FSMAdmin.calVoLud)
	dp.register_message_handler(load_date, state = FSMAdmin.date)
	dp.register_message_handler(load_Telefon, state = FSMAdmin.Telefon)
	dp.register_message_handler(load_name, state = FSMAdmin.Name)
	dp.register_message_handler(mрK_start, text =['Бронь комнаты'], state=None)
	dp.register_message_handler(load_calVoLudK, state=FSMAdminK.calVoLudK)
	dp.register_message_handler(load_dateK, state = FSMAdminK.date)
	dp.register_message_handler(load_TelefonK, state = FSMAdminK.Telefon)
	dp.register_message_handler(load_nameK, state = FSMAdminK.Name)
